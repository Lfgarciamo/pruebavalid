package com.example.valid.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.valid.common.dto.PersonaDTO;
import com.example.valid.common.entity.PersonaEntity;
import com.example.valid.repository.IPersonaRepository;

@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	private IPersonaRepository iPersonaRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public ResponseEntity crearPersona(PersonaDTO personaDTO) {

		try {
			PersonaEntity persona = modelMapper.map(personaDTO, PersonaEntity.class);
			PersonaDTO response = modelMapper.map(iPersonaRepository.crearUsuario(persona), PersonaDTO.class);
			return ResponseEntity.status(HttpStatus.CREATED).body(response);
		} catch (DataIntegrityViolationException e) {

			return ResponseEntity.ok("Error llave integridad de datos violada");
		} catch (Exception e) {

			return ResponseEntity.ok(e.getMessage());

		}
	}

	@Override
	public ResponseEntity consultarPersonaById(int id) {

		try {
			PersonaEntity personaResponse = iPersonaRepository.consultarUsuarioById(id);
			PersonaDTO personaDto = null;
			if (!ObjectUtils.isEmpty(personaResponse)) {
				personaDto = modelMapper.map(personaResponse, PersonaDTO.class);
			}
			HttpStatus status = (ObjectUtils.isEmpty(personaDto)) ? HttpStatus.NOT_FOUND : HttpStatus.OK;
			Object objResponse = (ObjectUtils.isEmpty(personaDto)) ? "Persona Not found" : personaDto;

			return ResponseEntity.status(status).body(objResponse);

		} catch (Exception e) {

			return ResponseEntity.status(HttpStatus.OK).body(e.getLocalizedMessage());
		}
	}

	@Override
	public ResponseEntity consultarPersonas() {
		return ResponseEntity.ok(iPersonaRepository.consultarPersonas());
	}

	@Override
	public ResponseEntity actualizarPersonas(List<PersonaEntity> personas) {
		iPersonaRepository.actualizarPersonas(personas);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
