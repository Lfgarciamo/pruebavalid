package com.example.valid.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.valid.common.entity.PersonaEntity;

@Repository
public class PersonaRepositoryImpl implements IPersonaRepository {

	@Autowired
	private IPersonaJpaRepository iPersonaJpaRepository;

	@Override
	public PersonaEntity crearUsuario(PersonaEntity persona)  throws Exception {
		
		  Optional<PersonaEntity> response = Optional.of(iPersonaJpaRepository.save(persona));

	        if (response.isPresent()) {
	            return response.get();
	        } else {
	            throw new Exception("No se pudo crear el registro");
	        }
	}

	@Override
	public PersonaEntity consultarUsuarioById(int id) {
		
		  Optional<PersonaEntity> response = iPersonaJpaRepository.findById(id);
	        if (response.isPresent()) {
	            return response.get();
	        } else {
	           return null;
	        }		
	}

	@Override
	public List<PersonaEntity> consultarPersonas() {
		List<PersonaEntity> result = new ArrayList<PersonaEntity>();
		iPersonaJpaRepository.findAll().forEach(result::add);
		return result;
	}

	@Override
	public void actualizarPersonas(List<PersonaEntity> personas) {
		iPersonaJpaRepository.saveAll(personas);
		
	}
}
