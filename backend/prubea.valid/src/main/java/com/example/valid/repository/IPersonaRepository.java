package com.example.valid.repository;

import java.util.List;

import com.example.valid.common.entity.PersonaEntity;

public interface IPersonaRepository {

    PersonaEntity crearUsuario(PersonaEntity persona) throws Exception;
    PersonaEntity consultarUsuarioById(int id);
    List<PersonaEntity> consultarPersonas();
    void actualizarPersonas(List<PersonaEntity> personas);
}
