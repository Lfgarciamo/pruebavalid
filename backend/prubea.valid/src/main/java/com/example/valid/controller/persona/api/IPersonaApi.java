package com.example.valid.controller.persona.api;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.valid.common.dto.PersonaDTO;
import com.example.valid.common.entity.PersonaEntity;

public interface IPersonaApi {
	
	 ResponseEntity crearPersona(PersonaDTO persona);
	 ResponseEntity consultarPersonaById(int id);
	 ResponseEntity consultarPersonas();
	 ResponseEntity actualizarPersonas(List<PersonaEntity> personas);
	

}
