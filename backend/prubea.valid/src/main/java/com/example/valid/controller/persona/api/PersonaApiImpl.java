package com.example.valid.controller.persona.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.valid.common.dto.PersonaDTO;
import com.example.valid.common.entity.PersonaEntity;
import com.example.valid.service.IPersonaService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("api/v1/personas")
public class PersonaApiImpl implements IPersonaApi {

    @Autowired
    private IPersonaService iPersonaService;

    /**
     * Servicio que usa el metodo HTTTP Post para registrar una nueva persona
     * @param persona datos de la persona a registrar
     * @return ResponseEntity con la respuesda de base de datos
     */
    @Override
    @PostMapping("")
    public ResponseEntity crearPersona(@RequestBody PersonaDTO persona) {
        return iPersonaService.crearPersona(persona);
    }

    /**
     * Servicio que usa el metodo HTTP Get para consultar una persona en base de datos
     * @param id identificador en base de datos de la persona a consultar
     * @return ResponseEntity con la respuesda de base de datos
     */
    @Override
    @GetMapping("/{id}")
    public ResponseEntity consultarPersonaById(@PathVariable("id") int id) {
        return iPersonaService.consultarPersonaById(id);
    }
    
    /**
     * Servicio que usa el metodo HTTP Get para consultar las persona en base de datos
     */
	@Override
	@GetMapping("")
	public ResponseEntity consultarPersonas() {
		return iPersonaService.consultarPersonas();
	}

	/**
	 * Servicio que permite actualizar una lista de personas
	 * @param personas Lista de personas a actualizar
	 */
	@Override
	@PutMapping("")
	public ResponseEntity actualizarPersonas(@RequestBody List<PersonaEntity> personas) {
		// TODO Auto-generated method stub
		return iPersonaService.actualizarPersonas(personas);
	}
    
    

}