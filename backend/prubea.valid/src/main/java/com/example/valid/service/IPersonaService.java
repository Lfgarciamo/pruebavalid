package com.example.valid.service;


import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.valid.common.dto.PersonaDTO;
import com.example.valid.common.entity.PersonaEntity;


public interface IPersonaService {

    ResponseEntity crearPersona(PersonaDTO registro);
    ResponseEntity consultarPersonaById(int id);
    ResponseEntity consultarPersonas(); 
    ResponseEntity actualizarPersonas(List<PersonaEntity> personas);
}
