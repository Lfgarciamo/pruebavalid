package com.example.valid.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.example.valid.common.entity.PersonaEntity;

public interface IPersonaJpaRepository extends JpaRepository<PersonaEntity,Integer> {

	
	
}
