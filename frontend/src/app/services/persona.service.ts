import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})


export class PersonaService {

  constructor(private http: HttpClient) { }

  postPersona(persona): Observable<any> {
    return this.http.post<any>("http://localhost:8080/api/v1/personas/", persona,httpOptions);
  }

  getPersonas(): Observable<any> {
    return this.http.get<any>("http://localhost:8080/api/v1/personas/",httpOptions);
  }

  putPersonas(personas): Observable<any> {
    return this.http.put<any>("http://localhost:8080/api/v1/personas/",personas);
  }
}



