import { Component, OnInit } from '@angular/core';
import {PersonaService} from '../../services/persona.service';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-persona-list',
  templateUrl: './persona-list.component.html',
  styleUrls: ['./persona-list.component.css']
})
export class PersonaListComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource : any;
  
  updates: any[];

  constructor(private personaService: PersonaService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getPersonas();

  }
  openSnackBar() {
   
    this._snackBar.open('Se actualizaron los registros','',{
      duration: 3000
    });
  }

  getPersonas(){
    this.personaService.getPersonas().subscribe(
      response=>{
        this.dataSource = response;
        console.log(response);
        
      }
    );
  }

  onClick(){
    console.log(this.dataSource);
    this.personaService.putPersonas(this.dataSource).subscribe(
      response=>{
          console.log(response); 
          this.openSnackBar();      
      },
      err =>{
        alert(err.message);
      }
    );
  }

}
