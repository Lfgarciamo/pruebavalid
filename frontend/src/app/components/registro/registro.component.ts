import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PersonaService } from '../../services/persona.service';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  crearPersonaForm : FormGroup;
  constructor( private fb: FormBuilder, 
    private personaService: PersonaService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {

    this.crearPersonaForm = this.fb.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]]    });
  }

  openSnackBar() {
   
    this._snackBar.open('Se registro la persona','',{
      duration: 3000
    });
  }
  onSubmit(){
    if (this.crearPersonaForm.valid) {
      let data = {
        nombre: this.crearPersonaForm.value.nombre,
        apellido: this.crearPersonaForm.value.apellido
      }

      this.personaService.postPersona(data).subscribe(result => {
        console.log(result);    
        this.openSnackBar();    
      },err=>{
        alert("Error");
      });
      console.log(data);
    }else{
      //alert("no valid");
    }
  }

}
